import os
import numpy as np
import networkx as nx
from torch.utils.data import Dataset

np.random.seed(12345)


def obtain_networkx_graph(dataset_path):
    all_edges = []
    nodes = []
    with open(os.path.join(dataset_path), 'r') as f:
        for line in f:
            a = line.split("\n")[0].split(" ")[0]
            b = line.split("\n")[0].split(" ")[1]
            all_edges.append((int(a), int(b)))
            nodes.append(int(a))
            nodes.append(int(b))

    G = nx.Graph()
    G.add_edges_from(all_edges)

    return G


def obtain_shortest_path(networkx_graph, source_node, destination_node):
    all_paths = []
    iterator = nx.all_shortest_paths(networkx_graph, source_node, destination_node)
    for path in iterator:
        all_paths.append(path)
    # print(all_paths)
    choice = np.random.randint(len(all_paths))
    return list(all_paths[choice])


def obtain_raw_path_dataset(main_graph, sample_per_node=25):
    all_nodes = sorted(list(main_graph.nodes))

    # dataset_paths = []
    # for i in range(200000):
    #     if i% 100 == 0:
    #         print(i)
    #     source = np.random.randint(low=0, high=len(all_nodes))
    #     dest = np.random.randint(low=0, high=len(all_nodes))
    #
    #     if nx.has_path(main_graph, source=source, target=dest):
    #         path = obtain_shortest_path(main_graph, source_node=source, destination_node=dest)
    #         dataset_paths.append(path)
    #
    # print(len(dataset_paths))
    # return dataset_paths

    # dataset_paths = []
    # for i in range(len(all_nodes)):
    #     node_1 = all_nodes[i]
    #     print(node_1)
    #     visited_nodes = set()
    #     for j in range(i + 1, len(all_nodes)):
    #         node_2 = all_nodes[j]
    #         p = np.random.uniform()
    #         if p <= 0.01 and nx.has_path(main_graph, node_1, node_2):
    #             path = obtain_shortest_path(main_graph, source_node=node_1, destination_node=node_2)
    #             dataset_paths.append(path)
    #             for k in path:
    #                 visited_nodes.add(k)
    #
    #     visited_nodes.clear()
    #
    # print(len(dataset_paths))
    # return dataset_paths

    dataset_paths = []
    no_nodes = len(all_nodes)
    for i in range(no_nodes):
        node_1 = all_nodes[i]
        if i % 100 == 0:
            print(i)
        for j in range(sample_per_node):
            ri = np.random.randint(no_nodes)
            node_2 = all_nodes[ri]
            if nx.has_path(main_graph, node_1, node_2):
                path = obtain_shortest_path(main_graph, source_node=node_1, destination_node=node_2)
                dataset_paths.append(path)

    print(len(dataset_paths))
    return dataset_paths


def obtain_dataset_dnn(main_graph, sample_per_node=50):
    all_nodes = sorted(list(main_graph.nodes))
    training_dnn = []
    no_nodes = len(all_nodes)
    for i in range(no_nodes):
        if i % 100 == 0:
            print(i)
        node_1 = all_nodes[i]
        for j in range(sample_per_node):
            node_2 = node_1
            while node_2 == node_1:
                ri = np.random.randint(no_nodes)
                node_2 = all_nodes[ri]
            if nx.has_path(main_graph, node_1, node_2):
                path = obtain_shortest_path(main_graph, source_node=node_1, destination_node=node_2)
                training_dnn.append([node_1, node_2, 1, int(path[1])])
            else:
                training_dnn.append([node_1, node_2, 0, -1])

    return training_dnn


class dataset_lstm(object):
    def __init__(self, input_seqs, input_seq_lengths, pad_id=0):
        self.input_seqs = input_seqs
        self.input_seq_lengths = input_seq_lengths

        self.ds_length = len(input_seqs)
        self.max_seq_length = np.max(input_seq_lengths)
        self.pad_id = 0

    def process_batch(self, inp_batch, inp_seq_batch):

        sd_batch_final = [[ls[0], ls[-1]] for ls in inp_batch]

        inp_seq_batch_final = [ln - 1 for ln in inp_seq_batch]
        max_length_batch = np.max(inp_seq_batch)

        inp_batch_final = [ls[:-1] + [self.pad_id] * (max_length_batch - len(ls)) for ls in inp_batch]
        tgt_batch_final = [ls[1:] + [self.pad_id] * (max_length_batch - len(ls)) for ls in inp_batch]

        zp = zip(inp_batch_final, inp_seq_batch_final, tgt_batch_final, sd_batch_final)
        zp_l = list(zp)

        zp_l = sorted(zp_l, key=lambda tuple: tuple[1], reverse=True)

        ib, sb, tb, sdb = zip(*zp_l)
        ib = list(ib)
        sb = list(sb)
        tb = list(tb)
        sdb = list(sdb)

        ib = np.array(ib)
        sb = np.array(sb)
        tb = np.array(tb)
        sdb = np.array(sdb)

        return ib, sb, tb, sdb

    def no_training_batches(self, batch_size):

        no_seqs = self.ds_length
        no_batches = int(np.ceil(no_seqs / batch_size))

        return no_batches

    def __call__(self, step, batch_size):

        no_seqs = self.ds_length
        no_batches = int(np.ceil(no_seqs / batch_size))

        step_no = step % no_batches

        if (step_no + 1) * batch_size > no_seqs:
            ts_pad = [self.input_seqs[itr % no_seqs] for itr in range((step_no + 1) * batch_size - no_seqs)]
            tsl_pad = [self.input_seq_lengths[itr % no_seqs] for itr in range((step_no + 1) * batch_size - no_seqs)]

            inp_batch = self.input_seqs[step_no * batch_size:] + ts_pad
            inp_seq_batch = self.input_seq_lengths[step_no * batch_size:] + tsl_pad

        else:
            inp_batch = self.input_seqs[step_no * batch_size:(step_no + 1) * batch_size]
            inp_seq_batch = self.input_seq_lengths[step_no * batch_size:(step_no + 1) * batch_size]

        return self.process_batch(inp_batch, inp_seq_batch)


class dataset_dnn(Dataset):

    def __init__(self, data):
        self.x = data[:, :-1]
        self.y = data[:, -1]

    def set_x(self, new_x):
        self.x = new_x

    def get_x(self):
        return self.x

    def set_y(self, new_y):
        self.y = new_y

    def get_y(self):
        return self.y

    def get_feature_shape(self):
        return self.x.shape[1]

    def get_num_classes(self):
        return np.max(self.y) + 2

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):
        return np.array(self.x[idx]).astype(np.int64), np.array(self.y[idx]).astype(np.int64)
