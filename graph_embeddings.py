import os

from node2vec import Node2Vec
import parameters
import numpy as np


def obtain_node2Vec_embeddings(networkx_graph, file_name, embedding_name):
    node2vec = Node2Vec(networkx_graph, dimensions=parameters.embedding_dimensions,
                        walk_length=parameters.embedding_walk_length,
                        num_walks=parameters.embedding_num_walks, workers=1)
    model = node2vec.fit(window=10, min_count=1, batch_words=4)
    model.wv.save_word2vec_format(os.path.join('models_' + embedding_name,
                                               'embedding_' + str(str(file_name).split(".")[0])))


def load_embeddings(vocab_to_int, embedding_name):
    embedding_dict = dict()
    with open(os.path.join('models_' + embedding_name, 'embedding_' + embedding_name), mode='r') as f:
        counter = 0
        for line in f:
            if counter >= 1:
                str_s = line.split("\n")[0].split(" ")
                vertex = int(str_s[0])
                vector = [float(str_s[i]) for i in range(1, len(str_s))]
                embedding_dict[vocab_to_int[vertex]] = vector
            counter += 1

    embedding_np = []
    for i in range(counter - 1):
        embedding_np.append(embedding_dict[i])
    embedding_np = np.array(embedding_np)
    return embedding_np
