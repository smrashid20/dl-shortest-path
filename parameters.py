embedding_dimensions = 64
embedding_walk_length = 30
embedding_num_walks = 200

lstm_model_batch_size = 32
lstm_model_hidden_size = 32
