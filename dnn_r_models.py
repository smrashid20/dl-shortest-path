import os

import torch
from torch import nn
from torch.utils.data import DataLoader

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class dnn_predictor(nn.Module):
    def __init__(self, no_nodes, pretrained_node_embeddings, no_output):
        super(dnn_predictor, self).__init__()

        self.embedding = nn.Embedding(no_nodes,
                                      embedding_dim=
                                      pretrained_node_embeddings.shape[1]).from_pretrained(pretrained_node_embeddings)

        self.embedding_dim = pretrained_node_embeddings.shape[1]
        self.vocab_size = pretrained_node_embeddings.shape[0]

        self.linear_inp_size = 2 * self.embedding_dim
        self.fc1 = nn.Linear(self.linear_inp_size, 32)
        self.fc2 = nn.Linear(32, 16)
        self.fc3 = nn.Linear(16, no_output)

    def forward(self, x):
        x_emb = self.embedding(x)
        x_emb = torch.reshape(x_emb, shape=[x.shape[0], -1])

        out_1 = torch.relu(self.fc1(x_emb))
        out_2 = torch.relu(self.fc2(out_1))
        out = self.fc3(out_2)

        return torch.squeeze(out)


def loss_fn(pred, target):
    return torch.nn.CrossEntropyLoss()(pred, target)


def train_dnn(dataset, save_path, model, epochs=100):
    training_data_length = int(0.8 * dataset.__len__())
    validation_data_length = dataset.__len__() - training_data_length
    training_data, validation_data = torch.utils.data.random_split(dataset,
                                                                   [training_data_length, validation_data_length])

    train_loader = DataLoader(dataset=training_data, batch_size=32, shuffle=True)
    validation_loader = DataLoader(dataset=validation_data, batch_size=32, shuffle=True)

    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    min_train_loss = 1000000
    min_validation_loss = 1000000

    for epoch in range(epochs):

        train_loss = 0.0
        train_num_correct = 0
        train_num_examples = 0
        train_batch_num = 0

        model.train()
        for batch_idx, (x, y_t) in enumerate(train_loader):
            x = torch.LongTensor(x).to(device)
            y = torch.LongTensor(y_t).to(device)

            train_batch_num = batch_idx
            optimizer.zero_grad()

            y_pred = model(x).to(device)

            loss = torch.nn.CrossEntropyLoss()(y_pred, y)
            train_loss += loss.item()

            loss.backward()
            optimizer.step()

            correct = torch.eq(torch.max(torch.softmax(y_pred,
                                                       dim=-1), dim=1)[1], y).view(-1)
            train_num_correct += torch.sum(correct).item()
            train_num_examples += correct.shape[0]

        model.eval()
        validation_loss = 0.0
        validation_num_correct = 0
        validation_num_examples = 0
        validation_batch_num = 0

        for batch_idx, (x, y_t) in enumerate(validation_loader):
            x = torch.LongTensor(x).to(device)
            y = torch.LongTensor(y_t).to(device)

            validation_batch_num = batch_idx

            y_pred = model(x).to(device)
            loss = torch.nn.CrossEntropyLoss()(y_pred, y)
            validation_loss += loss.item()

            correct = torch.eq(torch.max(torch.softmax(y_pred,
                                                       dim=-1), dim=1)[1], y).view(-1)

            validation_num_correct += torch.sum(correct).item()
            validation_num_examples += correct.shape[0]

        train_loss /= (train_batch_num + 1)
        train_acc = train_num_correct / train_num_examples

        validation_loss /= (validation_batch_num + 1)
        validation_acc = validation_num_correct / validation_num_examples

        if epoch % 1 == 0:
            print("epoch {}; T loss={:.4f} T Accuracy={:.4f}; V loss={:.4f} V Accuracy={:.4f}".
                  format(epoch, train_loss, train_acc, validation_loss, validation_acc))

        if epoch == 0 or min_validation_loss > validation_loss:
            min_validation_loss = validation_loss
            torch.save(model.state_dict(), save_path)

    print("model saved to {}.".format(save_path))


def get_linkpredictor_dnn(dataset_linkpredictor, no_nodes,
                          embedding_name, pretrained_embeddings, load_from_file=True):
    pretrained_embeddings = torch.FloatTensor(pretrained_embeddings).to(device)
    linkpredictor_model = dnn_predictor(pretrained_node_embeddings=pretrained_embeddings,
                                        no_nodes=no_nodes,
                                        no_output=2).to(device)

    save_path = os.path.join("models_r_" + embedding_name, "dnn_link_f_" + embedding_name)

    if not load_from_file:
        print("\nLink Predictor")
        train_dnn(dataset=dataset_linkpredictor,
                  save_path=save_path,
                  model=linkpredictor_model,
                  epochs=20)

        print("\n")
    linkpredictor_state_dict = torch.load(save_path)
    linkpredictor_model.load_state_dict(linkpredictor_state_dict)
    # print_all(dataset=dataset, model=linkpredictor_model)
    return linkpredictor_model


def get_nodepredictor_dnn(dataset_nodepredictor, no_nodes,
                          embedding_name, pretrained_embeddings, load_from_file=True):
    pretrained_embeddings = torch.FloatTensor(pretrained_embeddings).to(device)
    nodepredictor_model = dnn_predictor(pretrained_node_embeddings=pretrained_embeddings,
                                        no_nodes=no_nodes,
                                        no_output=no_nodes).to(device)

    save_path = os.path.join("models_r_" + embedding_name, "dnn_node_f_" + embedding_name)

    if not load_from_file:
        print("\nNode Predictor")
        train_dnn(dataset=dataset_nodepredictor,
                  save_path=save_path,
                  model=nodepredictor_model,
                  epochs=100)

        print("\n")
    nodepredictor_state_dict = torch.load(save_path)
    nodepredictor_model.load_state_dict(nodepredictor_state_dict)
    # print_all(dataset=dataset, model=nodepredictor_model)
    return nodepredictor_model
