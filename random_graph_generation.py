import os

import networkx as nx


graph = nx.generators.dense_gnm_random_graph(n=333, m=2519, seed=12345)

with open(os.path.join('Dataset', 'random', '0.edges'), mode='w') as file:
    for edge in graph.edges:
        row = [str(str(edge[0]) + " " + str(edge[1]) + "\n")]
        file.writelines(row)
