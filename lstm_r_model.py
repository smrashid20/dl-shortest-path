import os

import torch
from torch import nn
import parameters
import numpy as np
import torch.optim as optim

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class path_predictor(nn.Module):
    def __init__(self, no_nodes, pretrained_node_embeddings, hidden_size):
        super(path_predictor, self).__init__()

        self.embedding = nn.Embedding(no_nodes,
                                      embedding_dim=
                                      pretrained_node_embeddings.shape[1]).from_pretrained(pretrained_node_embeddings)

        self.embedding_dim = pretrained_node_embeddings.shape[1]
        self.vocab_size = pretrained_node_embeddings.shape[0]
        self.hidden_size = hidden_size

        self.encoder = nn.LSTM(input_size=3 * self.embedding_dim,
                               hidden_size=self.hidden_size, num_layers=1, batch_first=True)

        self.linear_inp_size = self.hidden_size + 2 * self.embedding_dim
        self.fc1 = nn.Linear(self.linear_inp_size, 16)
        self.fc2 = nn.Linear(16, no_nodes)

    def forward(self, seq, seq_lengths, seq_sd):
        seq_sd = self.embedding(seq_sd)
        seq_sd = torch.reshape(seq_sd, shape=[seq.shape[0], 1, 2 * self.embedding_dim])
        seq_sd = seq_sd.repeat(1, seq.shape[1], 1)

        embeds = self.embedding(seq)
        embeds = torch.cat([embeds, seq_sd], dim=-1)

        lstm_input = nn.utils.rnn.pack_padded_sequence(embeds, seq_lengths.cpu(), batch_first=True)

        output, (hidden, _) = self.encoder(lstm_input)
        output, _ = nn.utils.rnn.pad_packed_sequence(output, batch_first=True)

        output_concat = torch.cat([output, seq_sd], dim=-1)

        out_1 = torch.relu(self.fc1(output_concat))
        out_2 = self.fc2(out_1)
        return out_2


def loss_fn(pred, target):
    return torch.nn.CrossEntropyLoss()(torch.transpose(pred, 1, 2), target)


def print_all(dataset, model):
    for i in range(dataset.no_training_batches(parameters.lstm_model_batch_size)):
        inputs, seq_lengths, targets, source_dest = dataset(i, parameters.lstm_model_batch_size)

        inputs = torch.LongTensor(inputs).to(device)
        seq_lengths = torch.LongTensor(seq_lengths).to(device)
        source_dest = torch.LongTensor(source_dest).to(device)

        targets = torch.LongTensor(targets).to(device)

        output = torch.softmax(model(inputs, seq_lengths, source_dest), dim=2)

        # op = torch.transpose(output, 0, 1)
        op = output
        op = op.cpu().detach().numpy()
        op = np.argmax(op, axis=2)

        # tgt = torch.transpose(targets, 0, 1)
        tgt = targets
        tgt = tgt.cpu().detach().numpy()

        print("PRED:")
        print(op)
        print("GT:")
        print(tgt)
        print("\n")


def train(dataset, embedding_name, model, optimizer, loss_fn_, epochs=100):
    validation_loss_min = 100000.0
    for epoch in range(epochs):
        training_loss = 0.0

        model.train()

        no_batches = 0
        for i in range(int(0.8 * dataset.no_training_batches(parameters.lstm_model_batch_size))):
            no_batches += 1
            inputs, seq_lengths, targets, source_dest = dataset(i, parameters.lstm_model_batch_size)
            inputs = torch.LongTensor(inputs).to(device)
            seq_lengths = torch.LongTensor(seq_lengths).to(device)
            source_dest = torch.LongTensor(source_dest).to(device)
            targets = torch.LongTensor(targets).to(device)

            optimizer.zero_grad()
            output = model(inputs, seq_lengths, source_dest)
            loss = loss_fn_(output, targets)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), 0.2)
            optimizer.step()

            training_loss += loss.data.item()

        training_loss /= no_batches

        validation_loss = 0.0
        model.eval()

        no_batches = 0
        for i in range(int(0.8 * dataset.no_training_batches(parameters.lstm_model_batch_size))
                , dataset.no_training_batches(parameters.lstm_model_batch_size)):
            no_batches += 1
            inputs, seq_lengths, targets, source_dest = dataset(i, parameters.lstm_model_batch_size)

            inputs = torch.LongTensor(inputs).to(device)
            seq_lengths = torch.LongTensor(seq_lengths).to(device)
            source_dest = torch.LongTensor(source_dest).to(device)
            targets = torch.LongTensor(targets).to(device)

            output = model(inputs, seq_lengths, source_dest)
            loss = loss_fn_(output, targets)

            validation_loss += loss.data.item()

        validation_loss /= no_batches

        if validation_loss < validation_loss_min:
            validation_loss_min = validation_loss
            torch.save(model.state_dict(), os.path.join("models_r_" + embedding_name, "LSTM_net_1_f_" + embedding_name))

        if epoch % 1 == 0 or epoch == epochs - 1:
            print('Epoch: {}, Training Loss: {:.3f}, Validation Loss: {:.3f}'.format
                  (epoch, training_loss, validation_loss))


def get_forward_lstm_model(dataset, no_nodes, embedding_name, pretrained_embeddings, load_from_file=True):
    pretrained_embeddings = torch.FloatTensor(pretrained_embeddings).to(device)
    path_predictor_model = path_predictor(pretrained_node_embeddings=pretrained_embeddings,
                                          no_nodes=no_nodes,
                                          hidden_size=parameters.lstm_model_hidden_size).to(device)
    if not load_from_file:
        optimizer_forward = optim.Adam(path_predictor_model.parameters(), lr=0.0008)

        print("\nLSTM Model (G)")
        train(dataset=dataset,
              embedding_name=embedding_name,
              model=path_predictor_model,
              optimizer=optimizer_forward,
              loss_fn_=loss_fn, epochs=80)

        print("\n")
    fwd_model_state_dict = torch.load(os.path.join("models_r_" + embedding_name, "LSTM_net_1_f_" + embedding_name))
    path_predictor_model.load_state_dict(fwd_model_state_dict)
    # print_all(dataset=dataset, model=path_predictor_model)
    return path_predictor_model
