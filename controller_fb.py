import os
from random import seed, randint

import networkx as nx
import torch
from memory_profiler import memory_usage
from time import process_time
from graph_embeddings import obtain_node2Vec_embeddings, load_embeddings
from dataset_preprocessing_fb import obtain_raw_path_dataset, obtain_networkx_graph, \
    dataset_lstm, obtain_dataset_dnn, dataset_dnn
from lstm_model import get_forward_lstm_model
from dnn_models import get_linkpredictor_dnn, get_nodepredictor_dnn, dnn_predictor
from sklearn.metrics import confusion_matrix, classification_report
from metrics import calc_pairsF1, calc_F1

import numpy as np
import pickle
import pandas as pd
import sys


def get_vocab_to_int(training_paths, all_nodes):
    all_vertices_freq_placeholder = dict()

    for i in range(len(training_paths)):
        for j in range(len(training_paths[i])):
            all_vertices_freq_placeholder.setdefault(training_paths[i][j], []).append(1)

    for i in range(len(all_nodes)):
        all_vertices_freq_placeholder.setdefault(all_nodes[i], []).append(1)

    for k, v in all_vertices_freq_placeholder.items():
        all_vertices_freq_placeholder[k] = len(v)

    all_vertices_freq_sorted = sorted(all_vertices_freq_placeholder.items(), key=lambda item: item[1], reverse=True)
    vocab_to_int = dict()

    for i in range(len(all_vertices_freq_sorted)):
        vocab_to_int[all_vertices_freq_sorted[i][0]] = i

    vocab_to_int['GO'] = len(all_vertices_freq_sorted)
    vocab_to_int['PAD'] = len(all_vertices_freq_sorted) + 1
    vocab_to_int['END'] = len(all_vertices_freq_sorted) + 2

    int_to_vocab = dict()
    for k, v in vocab_to_int.items():
        int_to_vocab[v] = k

    return vocab_to_int, int_to_vocab


def convert_vocab_to_int(paths, vocab_to_int):
    paths_new = []
    for path in paths:
        path_new = [vocab_to_int[i] for i in path]
        paths_new.append(path_new)

    return paths_new


def convert_int_to_vocab(paths, int_to_vocab):
    paths_new = []
    for path in paths:
        path_new = [int_to_vocab[i] for i in path]
        paths_new.append(path_new)

    return paths_new


def convert_vocab_to_int_dnn(training_dnn_raw, vocab_to_int):
    training_dnn_new = []
    for sample in training_dnn_raw:
        sample_new = sample.copy()
        sample_new[0] = vocab_to_int[sample_new[0]]
        sample_new[1] = vocab_to_int[sample_new[1]]
        if sample_new[3] != -1:
            sample_new[3] = vocab_to_int[sample_new[3]]
        training_dnn_new.append(sample_new)

    return training_dnn_new


def get_processed_dataset_lstm(main_graph_):
    all_data_train = obtain_raw_path_dataset(main_graph_,sample_per_node=25)
    vocab_to_int, int_to_vocab = get_vocab_to_int(all_data_train, list(main_graph_.nodes))
    all_data_train = convert_vocab_to_int(all_data_train, vocab_to_int)

    np.random.shuffle(all_data_train)
    all_data_train_seq = []

    all_train_final = []
    for j in range(len(all_data_train)):
        if len(all_data_train[j]) >= 2:
            all_train_final.append(all_data_train[j])
            all_data_train_seq.append(len(all_data_train[j]))

    return dataset_lstm(all_train_final, all_data_train_seq,
                        pad_id=vocab_to_int['PAD']), vocab_to_int, int_to_vocab


def get_processed_dataset_dnn(main_graph_, vocab_to_int):
    training_dnn_raw = obtain_dataset_dnn(main_graph_,sample_per_node=50)
    training_dnn = convert_vocab_to_int_dnn(training_dnn_raw=training_dnn_raw,
                                            vocab_to_int=vocab_to_int)

    training_dnn_linkpredictor = []
    training_dnn_nodepredictor = []
    for sample in training_dnn:
        if sample[2] == 1:
            training_dnn_nodepredictor.append([sample[0], sample[1], sample[3]])

        training_dnn_linkpredictor_sample = [sample[0], sample[1], sample[2]]
        training_dnn_linkpredictor.append(training_dnn_linkpredictor_sample)

    return dataset_dnn(np.array(training_dnn_linkpredictor)), \
           dataset_dnn(np.array(training_dnn_nodepredictor))


def obtain_path(vocab_to_int, int_to_vocab, no_nodes,
                lstm_model, nodepredictor_model, linkpredictor_model, source_node, destination_node):
    source_node = vocab_to_int[source_node]
    destination_node = vocab_to_int[destination_node]

    visited_mask = np.ones(no_nodes)

    x = torch.LongTensor([[source_node, destination_node]]).to(device)
    y_link = linkpredictor_model(x)
    y_link_pred = torch.argmax(y_link)
    y_link_pred_np = y_link_pred.cpu().detach().numpy()

    if y_link_pred_np == 1:
        path = [source_node]
        previous_node = source_node
        dest_prob = []

        for i in range(1, 10):
            inputs = [path]
            seq_lengths = [i]
            source_dest = [[source_node, destination_node]]

            inputs = torch.LongTensor(inputs).to(device)
            seq_lengths = torch.LongTensor(seq_lengths).to(device)
            source_dest = torch.LongTensor(source_dest).to(device)

            output = torch.softmax(lstm_model(inputs, seq_lengths, source_dest), dim=2)
            G = output[-1][-1]
            G = torch.log10(G)

            nodepredictor_x = torch.LongTensor([[previous_node, destination_node]]).to(device)

            H = torch.softmax(nodepredictor_model(nodepredictor_x), dim=-1)
            H = torch.log10(H)

            F = G + H
            F = F.cpu().detach().numpy()
            F = F * visited_mask

            node = np.argmax(F)
            dest_prob.append(F[destination_node])

            visited_mask[int(node)] *= 100000
            path.append(int(node))
            previous_node = int(node)
            if node == destination_node:
                break

        dest_prob_idx = np.argmax(dest_prob) + 1
        path = path[:dest_prob_idx] + [destination_node]
        path = [int_to_vocab[i] for i in path]

        return path

    else:
        return [source_node]


if __name__ == '__main__':
    embedding_name = sys.argv[1]
    np.random.seed(12345)
    graph = obtain_networkx_graph('Dataset/facebook/'+embedding_name+'.edges')
    no_nodes = len(graph.nodes)

    all_nodes = sorted(list(graph.nodes))

    if not os.path.exists('models_' + embedding_name):
        os.mkdir('models_' + embedding_name)

    # obtain_node2Vec_embeddings(graph, '0.edges')
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # dataset, vocab_to_int, int_to_vocab = get_processed_dataset_lstm(graph)
    #
    # with open(os.path.join('models_' + embedding_name, 'dataset_fb'), 'wb') as outp:
    #     pickle.dump(dataset, outp, pickle.HIGHEST_PROTOCOL)
    #
    # with open(os.path.join('models_' + embedding_name, 'vocab_to_int_fb'), 'wb') as outp:
    #     pickle.dump(vocab_to_int, outp, pickle.HIGHEST_PROTOCOL)
    #
    # with open(os.path.join('models_' + embedding_name, 'int_to_vocab_fb'), 'wb') as outp:
    #     pickle.dump(int_to_vocab, outp, pickle.HIGHEST_PROTOCOL)

    with open(os.path.join('models_' + embedding_name, 'dataset_fb'), 'rb') as outp:
        dataset = pickle.load(outp)

    with open(os.path.join('models_' + embedding_name, 'vocab_to_int_fb'), 'rb') as outp:
        vocab_to_int = pickle.load(outp)

    with open(os.path.join('models_' + embedding_name, 'int_to_vocab_fb'), 'rb') as outp:
        int_to_vocab = pickle.load(outp)

    node_embeddings = load_embeddings(
        vocab_to_int=vocab_to_int, embedding_name=embedding_name)

    # dataset_linkpredictor, dataset_nodepredictor = get_processed_dataset_dnn(graph, vocab_to_int)
    #
    # with open(os.path.join('models_' + embedding_name, 'dataset_linkpredictor'), 'wb') as outp:
    #     pickle.dump(dataset_linkpredictor, outp, pickle.HIGHEST_PROTOCOL)
    #
    # with open(os.path.join('models_' + embedding_name, 'dataset_nodepredictor'), 'wb') as outp:
    #     pickle.dump(dataset_nodepredictor, outp, pickle.HIGHEST_PROTOCOL)

    with open(os.path.join('models_' + embedding_name, 'dataset_linkpredictor'), 'rb') as outp:
        dataset_linkpredictor = pickle.load(outp)

    with open(os.path.join('models_' + embedding_name, 'dataset_nodepredictor'), 'rb') as outp:
        dataset_nodepredictor = pickle.load(outp)

    lstm_model = get_forward_lstm_model(dataset=dataset,
                                        no_nodes=no_nodes,
                                        embedding_name=embedding_name,
                                        pretrained_embeddings=node_embeddings,
                                        load_from_file=True)

    linkpredictor_model = get_linkpredictor_dnn(dataset_linkpredictor=dataset_linkpredictor,
                                                no_nodes=no_nodes,
                                                embedding_name=embedding_name,
                                                pretrained_embeddings=node_embeddings,
                                                load_from_file=True)

    nodepredictor_model = get_nodepredictor_dnn(dataset_nodepredictor=dataset_nodepredictor,
                                                no_nodes=no_nodes,
                                                embedding_name=embedding_name,
                                                pretrained_embeddings=node_embeddings,
                                                load_from_file=True)

    print("Inference\n\n\n")
    # start_time = process_time()
    #
    # ret_tuple = memory_usage((obtain_path, (vocab_to_int, int_to_vocab, no_nodes,
    #                                         lstm_model, nodepredictor_model,
    #                                         linkpredictor_model, 154, 81)),
    #                          interval=0.1,
    #                          max_usage=True, retval=True)
    #
    # end_time = process_time()
    # print("Time Taken:", end_time - start_time, "Seconds")
    # memory_consumption = ret_tuple[0]
    #
    # print("Memory Usage:", memory_consumption, "MB")
    # predicted_path = ret_tuple[1]
    # print(predicted_path)
    #
    # paths = nx.all_shortest_paths(graph, source=154, target=81)
    # for path in paths:
    #     print(path)


    data_dir = '../Dataset/facebook/'
    out_dir = './Results/'
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
    out_file_name = 'facebook_results_ouralgo.csv'
    shortest_path_folder = './Results/Shortest Paths (Facebook, Our Algo)/'
    if not os.path.isdir(shortest_path_folder):
        os.mkdir(shortest_path_folder)
    num_of_queries = 6

    # generate queries
    df = pd.DataFrame()  # for storing timing and memory info

    # following lists will be added to the DataFrame
    queries = []
    sources = []
    destinations = []
    times = []
    memory_consumptions = []
    shortest_distances = []
    shortest_paths = []

    ALL_GT_SHORTEST_PATHS = dict()
    ALL_PRED_SHORTEST_PATHS = dict()
    ALL_LINK_GT = []
    ALL_LINK_PRED = []
    all_mse_loss = []

    seed(100)  # seed to generate same random number for all algorithms
    for q in range(1, num_of_queries + 1):
        print("QUERY", q)
        queries.append(q)

        # generate random source and destination
        src = randint(0, no_nodes - 1)
        dst = randint(0, no_nodes - 1)
        while src == dst:  # so that src and dst is not same
            dst = randint(0, no_nodes - 1)

        src = all_nodes[src]
        dst = all_nodes[dst]

        print("Source:", src)
        print("Destination:", dst)
        sources.append(src)
        destinations.append(dst)

        # call the shortest path function
        start_time = process_time()
        ret_tuple = memory_usage((obtain_path, (vocab_to_int, int_to_vocab, no_nodes,
                                                lstm_model, nodepredictor_model,
                                                linkpredictor_model, src, dst)),
                                 interval=0.1,
                                 max_usage=True, retval=True)
        end_time = process_time()
        print("Time Taken:", end_time - start_time, "Seconds")
        times.append(end_time - start_time)

        memory_consumption = ret_tuple[0]
        shortest_dist = len(ret_tuple[1])
        shortest_path = ret_tuple[1]

        print("Memory Usage:", memory_consumption, "MB")
        memory_consumptions.append(memory_consumption)

        print("Shortest Distance:", shortest_dist)
        shortest_distances.append(shortest_dist)

        print("Path: ")
        print(shortest_path)

        if len(shortest_path) < 2:
            ALL_LINK_PRED.append(0)
        else:
            ALL_LINK_PRED.append(1)

        if nx.has_path(graph, source=src, target=dst):
            ALL_LINK_GT.append(1)
        else:
            ALL_LINK_GT.append(0)

        query_str = str(src) + "-" + str(dst)
        if nx.has_path(graph, source=src, target=dst) and len(shortest_path) >= 2:
            all_gt_path = []
            paths = nx.all_shortest_paths(graph, source=src, target=dst)
            for path in paths:
                all_gt_path.append(list(path))
            ALL_GT_SHORTEST_PATHS[query_str] = all_gt_path
            ALL_PRED_SHORTEST_PATHS[query_str] = list(shortest_path)
            all_mse_loss.append((len(shortest_path) - len(all_gt_path[0]))**2)

        # shortest path is saved in separate files
        shortest_path_file_name = shortest_path_folder + 'query' + str(q) + '.txt'
        f = open(shortest_path_file_name, 'wb')
        pickle.dump(shortest_path, f)
        f.close()
        shortest_paths.append(shortest_path_file_name)
        print('\n\n')

    # print(ALL_GT_SHORTEST_PATHS)
    # print(ALL_PRED_SHORTEST_PATHS)

    print(len(list(ALL_GT_SHORTEST_PATHS.keys())))

    print(confusion_matrix(np.array(ALL_LINK_GT), np.array(ALL_LINK_PRED)))
    print(classification_report(np.array(ALL_LINK_GT), np.array(ALL_LINK_PRED)))

    print("Average Time : {} ".format(np.average(times)))
    print("Average Memory : {}".format(np.average(memory_consumptions)))
    print("Average MSE : {}".format(np.average(all_mse_loss)))



    all_F1_scores = []
    for k, v in ALL_GT_SHORTEST_PATHS.items():
        max_F1 = 0
        path_pred = ALL_PRED_SHORTEST_PATHS[k]
        for path_gt in v:
            curr_f1 = calc_F1(path_gt, path_pred)
            if curr_f1 >= max_F1:
                max_F1 = curr_f1

        all_F1_scores.append(max_F1)

    print(all_F1_scores)
    print("Average F1 : {}".format(np.average(all_F1_scores)))

    all_PF1_scores = []
    for k, v in ALL_GT_SHORTEST_PATHS.items():
        max_PF1 = 0
        path_pred = ALL_PRED_SHORTEST_PATHS[k]
        for path_gt in v:
            curr_pf1 = calc_pairsF1(path_gt, path_pred)
            if curr_pf1 >= max_PF1:
                max_PF1 = curr_pf1

        all_PF1_scores.append(max_PF1)

    print(all_PF1_scores)
    print("Average PF1 : {}".format(np.average(all_PF1_scores)))

    df['Query'] = pd.Series(queries)
    df['Source'] = pd.Series(sources)
    df['Destination'] = pd.Series(destinations)
    df['Time (sec)'] = pd.Series(times)
    df['Memory (MB)'] = pd.Series(memory_consumptions)
    df['Shortest Distance'] = pd.Series(shortest_distances)
    df['Shortest Path'] = pd.Series(shortest_paths)
    df.to_csv(out_dir + out_file_name)
